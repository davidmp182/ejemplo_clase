JOIN POR CONDICION

select last_name, department_name from employees, departments 
where employees.department_id=departments.department_id
-------------------------------------------------------------------
INNER JOIN

select e.last_name, d.department_name from employees e 
join departments d on (e.department_id=d.department_id)

SOLO SE NECESITA QUE EL CAMPO EN COMUN TENGAN EL MISMO TIPO DE DATO

------------------------------------------------------------------
LEFT OUTER JOIN

select e.last_name, d.department_name from employees e 
left outer join departments d on (e.department_id=d.department_id)

ES LO MISMO AL DE ARRIBO SOLO QUE ME TRAE EL DEPARTMENT_ID QUE
NO TIENE DEPARTMENTS

-----------------------------------------------------------------
RIGHT OUTER JOIN

select e.last_name, d.department_name from employees e 
right outer join departments d on (e.department_id=d.department_id)

ES LO MISMO AL DE ARRIBO SOLO QUE ME TRAE EL DEPARTMENTS QUE
NO TIENE DEPARTMENT_ID
------------------------------------------------------------------
FULL OUTER JOIN


select e.last_name, d.department_name from employees e 
right outer join departments d on (e.department_id=d.department_id)

ES LO MISMO AL DE ARRIBO SOLO QUE ME TRAE EL DEPARTMENTS QUE
NO TIENE DEPARTMENT_ID Y ME TRAE EL DEPARTMENT_ID QUE
NO TIENE DEPARTMENTS

------------------------------------------------------------------------
USING

select d.department, l.street_adress from department d
join locations l using(location_id)

QUE TENGAN EL MISMO TIPO DE DATO Y NOMBRE 
------------------------------------------------------------------------
NATURAL JOIN

select department, street_adress from department 
natural join locations l 
-------------------------------------------------------------------------
CROSS JOIN 

select e.last_name,d.department_id,d.department_name 
from employees e cross join departments d 
------------------------------------------------------------------------













