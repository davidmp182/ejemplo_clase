------------------------------------NUMERICOS--------------------------------------------------------

LOWER----- select lower(last_name), lower(first_name), salary from employees------ todo se ve en minuscula-------

UPPER----- select upper(last_name), upper(first_name), salary from employees------ todo se ve en mayuscula-----

INICAP--- select initcap(last_name), initcap(first_name), salary from employees----solo la primera letra se ve en mayucula--------


-------------------------------------CARACTERES------------------------------------------------------

CONCAT------select concat('Hola',last_name) from employees ----- me le concatena a last_name la palabra hola-----

SUBSTR----select substr('Hola Mundo', 5,3) from dual--- me muestra desde la posicion 5 3 letras y dual es para hacer ensayos con caracteres---


SUBSTR----select substr('Hola Mundo', -3) from dual--- me muestra desde 3 letras desde el final cuando son negativos y dual es para hacer ensayos con caracteres---

LENGTH-------select length(concat('Hola',last_name)), first_name from employees---- me cuenta cuantas letras hay o tambien numeros------

INSTR--------select last_name, salary from employees where instr(last_name,'e')>0----- me muestra los apellidos que tengan una E dentro de ellos y se afecta si es mayuscusla y minuscula------

LPAD y RPAD--select lpad(last_name,20,'*'), rpad(last_name,20,'*') from employees---- A�ade 20 caracteres (*)--------------

REPLACE------select replace(last_name,'A','W'),last_name from employees---- me cabia todas las A por A es sensible a mayuscula y minuscula-------

TRIM---------select trim('H' from 'Hola mundo H') from dual -----borra la h o la letra que indiques--


-----------------------------------DECIMALES---------------------------------------------------------

ROUND--------select round(3446.645,2) from dual ------sirve para quitar los decimales y redondear cuando yo indico un numero negatico (3446.645,-1) numeros negativo

TRUNC--------select trunc(34400.664,0) from dual -------sirve para redondear cuando el numero indicado es menos 0 redonde los numeros enteros y cuando es mayor que cero redondea los decimales

SQRT---------select sqrt(4) from dual -------saca la raiz cuadra

MOD----------select mod(8) from dual --------divide

POWER--------select power(3,4) from dual ----saca potenciacion

--------------------------------------------FECHA ---------------------------------------------------------
               siglo-a�o-mes-dia-hora-minutos-segundos
TO_CHAR-----select to_char(sysdate,'cc-yy-mm-dd-hh-mi-ss') from dual--------me convierte

select to_char(sysdate,'cc-yy-mm-dd-hh24:mi:ss') from dual

select to_char(sysdate) from dual---- me muestra la fecha

select sysdate from dual

select last_name,hire_date from employees where hire_date<'01-feb-88'------para comparar fechas tiene que ir ordenado en dia-mes-a�o y entre comillas
                   DD -MM- YY

TO_DATE---------select last_name,hire_date from employees where hire_date<to_date('01-feb-88')------ el to_date me froza a que me le como formato de fecha

select round(sysdate - (to_date('07-nov-96')),0) from dual -------- me trae los dia y los redondeo con round

select round(months_between(sysdate,'07-11-96')),0 from dual ---------------me trae los meses y los redondeo con round

select add_months(sysdate,6) from dual -------------------------------------me agraga meses a la fecah dada

select next_day(sysdate,'Lunes') from dual---------------------------------- me muestra el proximo lunes o se poner el numero del dia de la semana lunes=1, martes=2 etc

select sysdate+2 from dual ---------------------me suma dias a la fecha dada----------------

select trunc(sysdate,'month') from dual ------------------me redondea los dias hacia bajo 

select trunc(to_date('12-4-17'),'year') from dual---------me redonde los meses hacia bajo el to_date me abliga a oracle a cojerme la fecha 













